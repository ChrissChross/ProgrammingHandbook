[Android Programming](./androidProgramming.md)
## Allgemein

### Each Android app lives in its own security sandbox 

* The Android operating system is a multi-user Linux system in which each app is a different user
* The system assigns each app a unique Linux user ID
* Each process has its own VM, so an app's code runs in isolation from other apps
* Every app runs in its own Linux process

### Manifest File

* An app must declare all its components in this file
* Identifies any user permissions the app requires, such as Internet access or read-access to the user's contacts
* Declares the minimum API Level required by the app
* Declares hardware and software features used or required by the app, such as a camera, bluetooth services, or a multitouch screen
* Declares API libraries the app needs to be linked against (other than the Android framework APIs), such as the Google Maps library
