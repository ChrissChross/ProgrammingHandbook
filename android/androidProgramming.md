[Home](../Readme.md)
## Android Programming

1. [Allgemein](./androidAllgemein.md)
1. [Application Components](./applicationComponents.md)
1. [Project Structure](./projectStructure.md)
1. [Resources](./resources.md)
1. [Activity Lifecycle](./activityLifecycle.md)
1. [Charts](./charts.md)
1. [Room](./room.md)