[Android Programming](./androidProgramming.md)
## Application Components

| Element            | Beschreibung                                         |
| ------------------ | ---------------------------------------------------- |
| Activity           | User Interface und Interaction zwischen App und User |
| Services           | Hintergrundprozesse der App                          |
| Broadcast Receiver | Kommunikation zwischen OS und App                    |
| Content Provider   | Datenbank- und Datenmanagement                       |

#### Activity

- Single screen with a user interface
- Performs actions on the screen
- Implemented as a subclass of Activity

``` java
public class MyActivity extends Activity { }
```

#### Service

- Runs in the background to perform long-running operations
- E.g. play music while user is in another app / fetch data from a webservice without blocking user interaction
- Implemented as a subclass of Service

``` java
public class MyService extends Service { }
```

#### Broadcast Receiver

- Respond to broadcast messages from other applications or from the system
- E.g. applications can initiate broadcasts to let other applications know that some data has been downloaded
- Implemented as a subclass of BroadcastReceiver class and each message is broadcaster as an Intent object

``` java
public class MyReceiver  extends  BroadcastReceiver {
   public void onReceive(context,intent) { }
}
```

#### Content Provider

- Supplies data from one application to others on request
- Requests are handled by the methods of the ContentResolver class
- Data may be stored in the file system, the database or somewhere else entirely
- Implemented as a subclass of ContentProvider
- Must implement a standard set of APIs that enable other applications to perform transactions

``` java
public class MyContentProvider extends  ContentProvider {
   public void onCreate() { }
}
```

### Additional Components

| Komponente | Beschreibung                                                            |
| ---------- | ----------------------------------------------------------------------- |
| Fragments  | Represents a portion of user interface in an Activity                   |
| Views      | UI elements that are drawn on-screen including buttons, lists forms     |
| Layouts    | View hierarchies that control screen format and appearance of the views |
| Intents    | Messages wiring components together                                     |
| Resources  | External elements, such as strings, constants and drawable pictures     |
| Manifest   | Configuration file for the application                                  |