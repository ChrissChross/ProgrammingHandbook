[Android Programming](./androidProgramming.md)
# Charts
Um Diagramme in Android zu erstellen kann die kostenlose Java Android Library MPAndroidChart verwendet werden.

**GitHub:** [https://github.com/PhilJay/MPAndroidChart](https://github.com/PhilJay/MPAndroidChart)

**Dokumentation:** [https://weeklycoding.com/mpandroidchart-documentation/](https://weeklycoding.com/mpandroidchart-documentation/)

