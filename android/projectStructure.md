[Android Programming](./androidProgramming.md)
## Project Structure

While creating a new project you might have noticed the directories and files which have been created. These are not randomly thrown into the Hello-World. They follow a strict management order and all serve a specific purpose. The structure is essential for the project to compile and work properly.

### Generated directories and files

| Name                | Purpose                                                                                                      |
| ------------------- | ------------------------------------------------------------------------------------------------------------ |
| Java                | Java source files for the project, default one MainActivity.java file with one activity class for app launch |
| res/drawable-hdpi   | Drawable objects                                                                                             |
| res/layout          | Files that define user interfaces                                                                            |
| res/values          | Xml files containing collection of resources, such as strings and colour definitions                         |
| AndroidManifest.xml | Manifest file describing the fundamental characteristics of the app and its components                       |
| Build.gradle        | Auto generated file with all the gradle stuff                                                                |

