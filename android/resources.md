[Android Programming](./androidProgramming.md)
## Resources

While coding your all cool android app you will eventually hit the point where you need resources. The all mighty android programming world has you covered with that. There is a predefined structure and way to access resources. Resources are positioned under the /res directory.
`All hail the R`

### Directories

| Directory | Resource Type |
| --------- | ------------- |
| anim/      | XML files that define animations and accessed from the R.anim class |
| color/    | XML files that define a list of colors and accessed from the R.color class |
| drawable/ | Image files like .png, .jpg, .gif or XML files that are compiled into bitmaps, state lists, shapes, animation drawable and accessed from the R.drawable class |
| layout/ | XML files that define a user interface layout and accessed from the R.layout class |
| menu/ | XML files that define application menus, such as an Options Menu, Context Menu, or Sub Menu and accessed from the R.menu class |
| raw/ | Arbitrary files to save in their raw form. You need to call Resources.openRawResource() with the resource ID, which is R.raw.filename to open such raw files |
| values/ | XML files that contain simple values, such as strings, integers, and colors and accessed by R.\<type\> class |
| xml/ | Arbitrary XML files that can be read at runtime by calling Resources.getXML(). You can save various configuration files here which will be used at run time. |

