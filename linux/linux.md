[Home](../Readme.md)
## Linux

### Root directory



| Ordner      | Beschreibung                                                                      |
| ----------- | --------------------------------------------------------------------------------- |
| /bin        | binaries, alle Programme wie z.B. ls, cat, ...                                    |
| /sbin       | system binaries, die im single user mode benötigt werden                          |
| /boot       | alles was das System zum booten braucht wie z.B. bootloader                       |
| /dev        | devices, die ganze hardware als files                                             |
| /etc        | et cetera / edit to config, system wide configuration files                       |
| /home       | folders for all the users                                                         |
| /lib(32/64) | libraries for the applications in bin/sbin                                        |
| /media      | automatically mounted drives                                                      |
| /mnt        | manually mounted drives                                                           |
| /opt        | optional                                                                          |
| /proc       | sudo files for system processes and software                                      |
| /root       | root user home folder                                                             |
| /run        | tmpfs file system that runs in ram                                                |
| /srv        | services                                                                          |
| /sys        | system, to interact with the kernel                                               |
| /tmp        | temporary, applications can store files there temporary                           |
| /usr        | user application space, applications used by the user, non essential applications |
| /var        | variable directory,                                                               |